var buf = "";

export function bufferGet() {
  return buf;
}

export function bufferReset() {
  buf = "";
}

export function bufferAddChar(ch) {
  buf += String.fromCharCode(ch);
}

brainfuck.as.wasm
=================

See live demo on <https://fperrad.frama.io/brainfuck.as.wasm/>.

The application is built with the language [Svelte](https://svelte.dev/).

The [Brainfuck](https://en.wikipedia.org/wiki/Brainfuck)
core is written in [AssemblyScript](https://www.assemblyscript.org/)
and compiled to [WebAssembly](https://webassembly.org/)
(See [file](assembly/brainfuck.ts)).

import { defineConfig } from 'vite'
import { svelte } from '@sveltejs/vite-plugin-svelte'

// https://vitejs.dev/config/
export default defineConfig({
  base: '/brainfuck.as.wasm/',
  build: {
    target: 'esnext' // await
  },
  plugins: [svelte()],
})

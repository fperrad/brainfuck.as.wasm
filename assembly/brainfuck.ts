@external('../src/lib/buffer.js', 'bufferAddChar')
declare function bufferAddChar(c: i32): void;

export function evalBf(prg: Uint8Array): void {
  const mem = new StaticArray<i32>(30000);
  let ptr: i32 = 0;
  let ip: i32 = 0;
  const len: i32 = prg.length;

  while (ip < len) {
    switch (prg[ip]) {
      case 62: // '>'
        ++ptr;
        break;
      case 60: // '<'
        --ptr;
        break;
      case 43: // '+'
        ++mem[ptr];
        break;
      case 45: // '-'
        --mem[ptr];
        break;
      case 46: // '.'
        bufferAddChar(mem[ptr]); // putc
        break;
      case 44: // ','
        // NYI: getc
        break;
      case 91: // '['
        if (mem[ptr] == 0) {
          let count: i32 = 1;
          while (count) {
            ++ip;
            if (prg[ip] == 91)
              // '['
              ++count;
            if (prg[ip] == 93)
              // ']'
              --count;
          }
        }
        break;
      case 93: // ']'
        if (mem[ptr] != 0) {
          let count: i32 = 1;
          while (count) {
            --ip;
            if (prg[ip] == 93)
              // ']'
              ++count;
            if (prg[ip] == 91)
              // '['
              --count;
          }
        }
        break;
      default:
        break;
    }
    ip++;
  }
}
